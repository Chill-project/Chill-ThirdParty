<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add "centers" and "active" to 3rd party
 */
final class Version20190418090842 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE chill_3party.party_center (thirdparty_id INT NOT NULL, center_id INT NOT NULL, PRIMARY KEY(thirdparty_id, center_id))');
        $this->addSql('CREATE INDEX IDX_C65D4397C7D3A8E6 ON chill_3party.party_center (thirdparty_id)');
        $this->addSql('CREATE INDEX IDX_C65D43975932F377 ON chill_3party.party_center (center_id)');
        $this->addSql('ALTER TABLE chill_3party.party_center ADD CONSTRAINT FK_C65D4397C7D3A8E6 FOREIGN KEY (thirdparty_id) REFERENCES chill_3party.third_party (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.party_center ADD CONSTRAINT FK_C65D43975932F377 FOREIGN KEY (center_id) REFERENCES centers (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_3party.third_party ADD active BOOLEAN NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE chill_3party.party_center');
        $this->addSql('ALTER TABLE chill_3party.third_party DROP active');
    }
}
