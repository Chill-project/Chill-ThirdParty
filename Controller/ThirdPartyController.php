<?php

namespace Chill\ThirdPartyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\ThirdPartyBundle\Security\Voter\ThirdPartyVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Chill\ThirdPartyBundle\Form\ThirdPartyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Role\Role;
use Chill\MainBundle\Pagination\PaginatorFactory;

/**
 * Routes for operations on ThirdParties.
 * 
 * @Route("/{_locale}/thirdparty/thirdparty")
 */
class ThirdPartyController extends Controller
{
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var PaginatorFactory
     */
    protected $paginatorFactory;
    
    public function __construct(
        AuthorizationHelper $authorizationHelper, 
        TranslatorInterface $translator,
        PaginatorFactory $paginatorFactory
    ) {
        $this->authorizationHelper = $authorizationHelper;
        $this->translator = $translator;
        $this->paginatorFactory = $paginatorFactory;
    }

    
    /**
     * @Route("/index", name="chill_3party_3party_index")
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(ThirdPartyVoter::SHOW);
        $repository = $this->getDoctrine()->getManager()
            ->getRepository(ThirdParty::class);
        
        $centers = $this->authorizationHelper
            ->getReachableCenters(
                $this->getUser(), 
                new Role(ThirdPartyVoter::SHOW)
                );
        
        $nbThirdParties = $repository->countByMemberOfCenters($centers);
        $pagination = $this->paginatorFactory->create($nbThirdParties);
        
        $pagination->setItemsPerPage(20);
        
        $thirdParties = $repository->findByMemberOfCenters(
            $centers,
            $pagination->getCurrentPage()->getFirstItemNumber(),
            $pagination->getItemsPerPage()
        );
        
        return $this->render('ChillThirdPartyBundle:ThirdParty:index.html.twig', array(
            'third_parties' => $thirdParties,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/new", name="chill_3party_3party_new")
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted(ThirdPartyVoter::CREATE);
        
        $centers = $this->authorizationHelper
            ->getReachableCenters(
                $this->getUser(), 
                new Role(ThirdPartyVoter::CREATE)
                );
        
        if (count($centers) === 0) {
            throw new \LogicException("There should be at least one center reachable "
                . "if role ".ThirdPartyVoter::CREATE." is granted");
        }
        
        $thirdParty = new ThirdParty();
        $thirdParty->setCenters(new ArrayCollection($centers));
        
        $form = $this->createForm(ThirdPartyType::class, $thirdParty, [
            'usage' => 'create'
        ]);
        $form->add('submit', SubmitType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($thirdParty);
            $em->flush();
            
            $this->addFlash('success',
                $this->translator->trans("Third party created")
                );
            
            return $this->redirectToRoute('chill_3party_3party_show', [
                'thirdparty_id' => $thirdParty->getId()
            ]);
            
        } elseif ($form->isSubmitted()) {
            $msg = $this->translator->trans('This form contains errors');
            $this->addFlash('error', $msg);
        }
        
        return $this->render('@ChillThirdParty/ThirdParty/new.html.twig', [
            'form' => $form->createView(),
            'thirdParty' => $thirdParty
        ]);
    }

    /**
     * @Route("/{thirdparty_id}/update", name="chill_3party_3party_update")
     * @ParamConverter("thirdParty", options={"id": "thirdparty_id"})
     */
    public function updateAction(ThirdParty $thirdParty, Request $request)
    {
       $this->denyAccessUnlessGranted(ThirdPartyVoter::CREATE);
        
        $centers = $this->authorizationHelper
            ->getReachableCenters(
                $this->getUser(), 
                new Role(ThirdPartyVoter::CREATE)
                );
        
        if (count($centers) === 0) {
            throw new \LogicException("There should be at least one center reachable "
                . "if role ".ThirdPartyVoter::CREATE." is granted");
        }
        
        // we want to keep centers the users has no access to. So we will add them
        // later if they are removed. (this is a ugly hack but it will works
        $centersAssociatedNotForUsers = \array_diff(
            $thirdParty->getCenters()->toArray(), 
            $centers);
        
        $form = $this->createForm(ThirdPartyType::class, $thirdParty, [
            'usage' => 'create'
        ]);
        $form->add('submit', SubmitType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            // re-add centers the user has no accesses:
            foreach ($centersAssociatedNotForUsers as $c) {
                $thirdParty->addCenter($c);
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            
            $this->addFlash('success',
                $this->translator->trans("Third party updated")
                );
            
            return $this->redirectToRoute('chill_3party_3party_show', [
                'thirdparty_id' => $thirdParty->getId()
            ]);
            
        } elseif ($form->isSubmitted()) {
            $msg = $this->translator->trans('This form contains errors');
            $this->addFlash('error', $msg);
        }
        
        return $this->render('@ChillThirdParty/ThirdParty/update.html.twig', [
            'form' => $form->createView(),
            'thirdParty' => $thirdParty
        ]);
    }
    
    /**
     * @Route("/{thirdparty_id}/show", name="chill_3party_3party_show")
     * @ParamConverter("thirdParty", options={"id": "thirdparty_id"})
     */
    public function showAction(ThirdParty $thirdParty, Request $request)
    {
        $this->denyAccessUnlessGranted(ThirdPartyVoter::SHOW, $thirdParty);
        
        return $this->render('@ChillThirdParty/ThirdParty/show.html.twig', [
            'thirdParty' => $thirdParty
        ]);
    }

}
