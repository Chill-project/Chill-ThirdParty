<?php
/*
 */
namespace Chill\ThirdPartyBundle\Form\ChoiceLoader;

use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;
use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Doctrine\ORM\EntityRepository;
use Chill\MainBundle\Entity\Center;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;

/**
 * Lazy load third parties.
 *
 */
class ThirdPartyChoiceLoader implements ChoiceLoaderInterface
{
    /**
     *
     * @var \Chill\ThirdPartyBundle\Entity\ThirdParty[]
     */
    protected $lazyLoadedParties = [];
    
    /**
     *
     * @var Center
     */
    protected $center;
    
    /**
     *
     * @var EntityRepository
     */
    protected $partyRepository;
    
    public function __construct(Center $center, EntityRepository $partyRepository)
    {
        $this->center = $center;
        $this->partyRepository = $partyRepository;
    }

    
    public function loadChoiceList($value = null): ChoiceListInterface
    {
        return new ArrayChoiceList($this->lazyLoadedParties, $value);
    }

    public function loadChoicesForValues($values, $value = null)
    {
        $choices = [];
        
        foreach($values as $value) {
            if (empty($value)) {
                continue;
            }
            
            $party = $this->partyRepository->find($value);
            
            if (FALSE === \in_array($this->center, $party->getCenters()->toArray())) {
                throw new \RuntimeException("the party's center is not authorized");
            }
            
            $choices[] = $party;
        }

        return $choices;
    }
    
    public function loadValuesForChoices(array $choices, $value = null)
    {
        $values = [];
        
        foreach ($choices as $choice) {
            if (NULL === $choice) {
                $values[] = null;
                continue;
            }
            
            $id = \call_user_func($value, $choice);
            $values[] = $id;
            $this->lazyLoadedParties[$id] = $choice;
        }
        
        return $values;
    }
}
