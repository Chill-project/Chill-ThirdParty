<?php
/*

 */
namespace Chill\ThirdPartyBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\MainBundle\Entity\User; 
use Symfony\Component\Security\Core\Role\Role;

/**
 * Voter for Third Party
 * 
 * 
 *
 */
class ThirdPartyVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    public const CREATE = 'CHILL_3PARTY_3PARTY_CREATE';
    public const UPDATE = 'CHILL_3PARTY_3PARTY_UPDATE';
    public const SHOW   = 'CHILL_3PARTY_3PARTY_SHOW';
    
    public function __construct(AuthorizationHelper $authorizationHelper)
    {
        $this->authorizationHelper = $authorizationHelper;
    }

    
    protected function supports($attribute, $subject)
    {
        if ($subject instanceof ThirdParty) {
            return \in_array($attribute, $this->getRoles());
        } elseif ($subject === NULL) {
            return $attribute === self::CREATE || $attribute === self::SHOW ;
        }
        
        return false;
    }
    
    /**
     * 
     * @param string $attribute
     * @param ThirdParty|null $subject
     * @param TokenInterface $token
     * @return type
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        
        if (!$user instanceof User) {
            return false;
        }
        
        $centers = $this->authorizationHelper
            ->getReachableCenters($user, new Role($attribute));
        
        if ($subject === NULL) {
            return count($centers) > 0;
        } elseif ($subject instanceof ThirdParty) {
            return count(\array_intersect($centers, $subject->getCenters()->toArray())) > 0;
        }
        
        return false;
    }

        public function getRoles(): array
    {
        return [
            self::CREATE, self::UPDATE, self::SHOW
        ];
    }

    public function getRolesWithHierarchy(): array
    {
        return [
            'Third Party' => $this->getRoles()
        ];
    }

    public function getRolesWithoutScope(): array
    {
        return $this->getRoles();
    }
}
