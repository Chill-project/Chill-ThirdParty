<?php

namespace Chill\ThirdPartyBundle\ThirdPartyType;

/**
 * Provide third party type
 */
interface ThirdPartyTypeProviderInterface
{
    /**
     * Return an unique key for this type.
     * 
     * @return string
     */
    public static function getKey(): string;
    
}
