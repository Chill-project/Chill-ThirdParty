<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2020 , Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\ThirdPartyBundle\Templating\Entity;

use Chill\MainBundle\Templating\Entity\AbstractChillEntityRender;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Symfony\Bridge\Twig\TwigEngine;

/**
 * 
 *
 */
class ThirdPartyRender extends AbstractChillEntityRender
{
    /**
     *
     * @var TwigEngine
     */
    protected $templating;
    
    public function __construct(TwigEngine $templating)
    {
        $this->templating = $templating;
    }
    
    /**
     * 
     * @param ThirdParty $entity
     * @param array $options
     * @return string
     */
    public function renderBox($entity, array $options): string
    {
        $params = \array_merge(
            [ 'with_valid_from' => true ],
            $options
            );
        
        return
            $this->getDefaultOpeningBox('_3party').
            $this->templating->render('@ChillThirdParty/ThirdParty/_render.html.twig', [ 
                'contact' => $entity, 
                'options' => $params
                ]).
            $this->getDefaultClosingBox();
            
    }

    /**
     * 
     * @param ThirdParty $entity
     * @param array $options
     * @return string
     */
    public function renderString($entity, array $options): string
    {
        return $entity->getName();
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof ThirdParty;
    }
}
