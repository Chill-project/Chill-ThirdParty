<?php

namespace Chill\ThirdPartyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\ThirdPartyBundle\Security\Voter\ThirdPartyVoter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Role\Role;
use Chill\ThirdPartyBundle\ThirdPartyType\ThirdPartyTypeManager;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Chill\MainBundle\Form\Type\AddressType;


class ThirdPartyType extends AbstractType
{
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    /**
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    
    /**
     *
     * @var ThirdPartyTypeManager
     */
    protected $typesManager;
    
    public function __construct(
        AuthorizationHelper $authorizationHelper, 
        TokenStorageInterface $tokenStorage,
        ThirdPartyTypeManager $typesManager
    ) {
        $this->authorizationHelper = $authorizationHelper;
        $this->tokenStorage = $tokenStorage;
        $this->typesManager = $typesManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $types = [];
        foreach ($this->typesManager->getProviders() as $key => $provider) {
            $types['chill_3party.key_label.'.$key] = $key;
        }
        
        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('telephone', TextType::class, [
                'label' => 'Phonenumber',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'required' => false
            ])
            ->add('comment', TextareaType::class, [
                'required' => false
            ])
            ->add('type', ChoiceType::class, [
                'choices' => $types,
                'expanded' => true,
                'multiple' => true,
                'label'    => 'thirdparty.Type'
            ])
            ->add('active', ChoiceType::class, [
                'choices' => [
                    'Active, shown to users' => true,
                    'Inactive, not shown to users' => false
                ],
                'expanded' => true,
                'multiple' => false
            ])
            ->add('centers', EntityType::class, [
                'choices' => $this->getReachableCenters($options),
                'class' => \Chill\MainBundle\Entity\Center::class,
                'multiple' => true,
                'expanded' => true
            ])
            ->add('address', AddressType::class, [
                'has_valid_from' => false,
                'null_if_empty' => true,
                'required' => false
            ])
            ;
    }
    
    /**
     * 
     * @param array $options
     * @return \Chill\MainBundle\Entity\Center[]
     */
    protected function getReachableCenters(array $options)
    {
        switch($options['usage']) {
            case 'create': $role = new Role(ThirdPartyVoter::CREATE);
                break;
            case 'update': $role = new Role(ThirdPartyVoter::UPDATE);
                break;
        }
        
        return $this->authorizationHelper->getReachableCenters(
            $this->tokenStorage->getToken()->getUser(), $role);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\ThirdPartyBundle\Entity\ThirdParty'
        ));
        
        $resolver->setRequired('usage')
            ->setAllowedValues('usage', ['create', 'update'])
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'chill_thirdpartybundle_thirdparty';
    }


}
