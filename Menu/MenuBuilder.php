<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2019, Champs Libres Cooperative SCRLFS,
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\ThirdPartyBundle\Menu;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Knp\Menu\MenuItem;
use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\ThirdPartyBundle\Security\Voter\ThirdPartyVoter;

/**
 * Add an entry in section to go to third party index page
 *
 */
class MenuBuilder implements LocalMenuBuilderInterface
{
    /**
     *
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TranslatorInterface $translator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    
    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if ($this->authorizationChecker->isGranted(ThirdPartyVoter::SHOW)) {
            $menu
                ->addChild(
                    $this->translator->trans('Third parties'), 
                    [
                        'route' => 'chill_3party_3party_index',
                    ])
                ->setExtras([
                    'order' => 112
                ])
                ;
        }
    }

    public static function getMenuIds(): array
    {
        return ['section'];
    }
}
