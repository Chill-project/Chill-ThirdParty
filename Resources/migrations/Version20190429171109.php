<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Alter third_party.type to jsonb and rename to types
 */
final class Version20190429171109 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE chill_3party.third_party RENAME COLUMN type "
            . "TO types");
        $this->addSql("ALTER TABLE chill_3party.third_party ALTER COLUMN types "
            . "SET DATA TYPE jsonb");

    }

    public function down(Schema $schema) : void
    {
        $this->throwIrreversibleMigrationException("not implemented");
    }
}
