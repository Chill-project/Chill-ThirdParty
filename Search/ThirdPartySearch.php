<?php
/*

 */
namespace Chill\ThirdPartyBundle\Search;

use Chill\MainBundle\Search\SearchInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\ThirdPartyBundle\Security\Voter\ThirdPartyVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Symfony\Component\Security\Core\Role\Role;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Chill\MainBundle\Pagination\PaginatorFactory;

/**
 * Allow to search amongst parties
 */
class ThirdPartySearch implements SearchInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    /**
     *
     * @var PaginatorFactory 
     */
    protected $paginatorFactory;
    
    const NAME = '3party';
    
    public function __construct(
        EntityManagerInterface $em, 
        TokenStorageInterface $tokenStorage, 
        AuthorizationHelper $authorizationHelper, 
        PaginatorFactory $paginatorFactory
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationHelper = $authorizationHelper;
        $this->paginatorFactory = $paginatorFactory;
    }

    
    public function getOrder(): int
    {
        return 59866;
    }

    public function isActiveByDefault(): bool
    {
        return false;
    }

    public function renderResult(array $terms, $start = 0, $limit = 50, $options = array(), $format = 'html')
    {
        $centers = $this->authorizationHelper
            ->getReachableCenters(
                $this->tokenStorage->getToken()->getUser(), 
                new Role(ThirdPartyVoter::SHOW)
                );
        $total = $this->count($centers, $terms);
        $paginator = $this->paginatorFactory->create($total);
        // replace types in terms by types in query
        $terms['types'] = $options[SearchInterface::REQUEST_QUERY_PARAMETERS]['t'];
        $terms['is_active'] = true;
        
        if ($format === 'json') {
            return [
                'results' => $this->em->getRepository(ThirdParty::class)
                    ->findByMemberOfCenters($centers, $start, $limit, $terms, 
                        ['array', ['tp.id', 'tp.name AS text']]),
                'more' => $paginator->hasNextPage()
            ];
        }
    }

    public function supports($domain, $format): bool
    {
        return self::NAME === $domain and $format === 'json';
    }
    
    protected function count($centers, $terms): int
    {
        return $this->em->getRepository(ThirdParty::class)
            ->countByMemberOfCenters($centers, $terms);
    }
}
