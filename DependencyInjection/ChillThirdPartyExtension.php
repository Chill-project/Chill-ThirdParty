<?php

namespace Chill\ThirdPartyBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\ThirdPartyBundle\Security\Voter\ThirdPartyVoter;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillThirdPartyExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->preprendRoutes($container);
        $this->prependRoleHierarchy($container);
    }
    
    protected function preprendRoutes(ContainerBuilder $container)
    {
        //declare routes for 3party bundle
        $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillThirdPartyBundle/Resources/config/routing.yml'
              )
           )
        ));
    }
    
    protected function prependRoleHierarchy(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
            'role_hierarchy' => array(
                ThirdPartyVoter::CREATE => [ThirdPartyVoter::SHOW],
                ThirdPartyVoter::UPDATE => [ThirdPartyVoter::SHOW],
            )
        ));
    }
}
