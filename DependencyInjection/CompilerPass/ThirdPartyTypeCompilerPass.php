<?php

namespace Chill\ThirdPartyBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Chill\ThirdPartyBundle\ThirdPartyType\ThirdPartyTypeManager;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Load services tagged chill_3party.provider and add them to the service 
 * definition of manager
 *
 */
class ThirdPartyTypeCompilerPass implements CompilerPassInterface
{
    const TAG = 'chill_3party.provider';
    
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition(ThirdPartyTypeManager::class);
        $usedKeys = [];
        
        foreach ($container->findTaggedServiceIds(self::TAG) as $id => $tags) {
            $taggedService = $container->getDefinition($id);
            // check forr keys already in use :
            $key = $taggedService->getClass()::getKey();
            if (\in_array($key, $usedKeys)) {
                throw new \LogicException(sprintf("Tag with key \"%s\" is already in used", 
                    $key));
            }
            $usedKeys[] = $key;
            // alter the service definition of manager
            $definition->addMethodCall('addProvider', [ new Reference($id) ]);
        }
    }
}
