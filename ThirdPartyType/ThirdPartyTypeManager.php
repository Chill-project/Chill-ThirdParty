<?php

namespace Chill\ThirdPartyBundle\ThirdPartyType;

/**
 * Manages types of third parties
 *
 */
class ThirdPartyTypeManager
{
    /**
     *
     * @var ThirdPartyTypeProviderInterface[]
     */
    protected $providers = [];
    
    /**
     * The prefix used to translate the key of provider
     */
    const THIRD_PARTY_TRANSLATOR_KEY = 'chill_3party.key_label.';
    
    /**
     * Add a provider to the manager
     * 
     * Method used during the load of the manager by Dependency Injection
     * 
     * @param \Chill\ThirdPartyBundle\ThirdPartyType\ThirdPartyTypeProviderInterface $provider
     * @return \self
     */
    public function addProvider(ThirdPartyTypeProviderInterface $provider): self
    {
        $this->providers[$provider->getKey()] = $provider;
        
        return $this;
    }
    
    /**
     * Get all providers
     * 
     * @return array
     */
    public function getProviders(): array
    {
        return $this->providers;
    }
    
    /**
     * Get a list of types
     * 
     * @return string[]
     */
    public function getTypes(): array
    {
        return \array_keys($this->providers);
    }
}
