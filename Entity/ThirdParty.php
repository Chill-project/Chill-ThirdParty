<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2019, Champs Libres Cooperative SCRLFS,
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ThirdPartyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Chill\MainBundle\Entity\Center;
use Symfony\Component\Validator\Constraints as Assert;
use Chill\MainBundle\Entity\Address;

/**
 * ThirdParty is a party recorded in the database.
 * 
 * A party may be attached to multiple centers. Being attach to a center allow 
 * all users with the right 'CHILL_3PARTY_3PARTY_SEE', 'CHILL_3PARTY_3 to see, select and edit parties for this 
 * center.
 *
 * @ORM\Table(name="chill_3party.third_party")
 * @ORM\Entity(repositoryClass="Chill\ThirdPartyBundle\Repository\ThirdPartyRepository")
 *
 */
class ThirdParty
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Length(min="2")
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=64, nullable=true)
     * @Assert\Regex("/^([\+{1}])([0-9\s*]{4,20})$/", 
     *   message="Invalid phone number: it should begin with the international prefix starting with ""+"", hold only digits and be smaller than 20 characters. Ex: +33123456789"
     * )
     */
    private $telephone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(checkMX=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var array|null
     *
     * @ORM\Column(name="types", type="json", nullable=true)
     * @Assert\Count(min=1)
     */
    private $type;
    
    /**
     *
     * @var boolean
     * @ORM\Column(name="active", type="boolean", options={"defaut": true})
     */
    private $active = true;
    
    /**
     *
     * @var Collection instances of Center
     * @ORM\ManyToMany(targetEntity="\Chill\MainBundle\Entity\Center")
     * @ORM\JoinTable(name="chill_3party.party_center")
     * @Assert\Count(min=1)
     */
    private $centers;
    
    /**
     *
     * @var Address|null
     * @ORM\ManyToOne(targetEntity="\Chill\MainBundle\Entity\Address",
     *   cascade={"persist", "remove"})
     * 
     */
    private $address;
    
    public function __construct()
    {
        $this->centers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ThirdParty
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set telephone.
     *
     * @param string|null $telephone
     *
     * @return ThirdParty
     */
    public function setTelephone($telephone = null)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string|null
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return ThirdParty
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return ThirdParty
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set type.
     *
     * @param array|null $type
     *
     * @return ThirdParty
     */
    public function setType(array $type = null)
    {
        // remove all keys from the input data
        $this->type = \array_values($type);

        return $this;
    }

    /**
     * Get type.
     *
     * @return array|null
     */
    public function getType()
    {
        return $this->type;
    }
    
    public function getActive(): bool
    {
        return $this->active;
    }

    public function getCenters(): Collection
    {
        return $this->centers;
    }

    public function setActive(bool $active)
    {
        $this->active = $active;
        return $this;
    }
    
    public function addCenter(Center $center)
    {
        if (FALSE === $this->centers->contains($center)) {
            $this->centers->add($center);
        }
    }
    
    public function removeCenter(Center $center)
    {
        if ($this->centers->contains($center)) {
            $this->centers->removeElement($center);
        }
    }

    public function setCenters(Collection $centers)
    {
        foreach ($centers as $center) {
            $this->addCenter($center);
        }
        
        foreach ($this->centers as $center) {
            if (FALSE === $centers->contains($center)) {
                $this->removeCenter($center);
            }
        }
        
        return $this;
    }
    
    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(Address $address)
    {
        $this->address = $address;
        
        return $this;
    }
    
    public function __toString()
    {
        return $this->getName();
    }
}
