<?php
/*
 * 
 */
namespace Chill\ThirdPartyBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\ThirdPartyBundle\Entity\ThirdParty;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\Options;
use Chill\ThirdPartyBundle\Form\ChoiceLoader\ThirdPartyChoiceLoader;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Chill\ThirdPartyBundle\Search\ThirdPartySearch;
use Chill\ThirdPartyBundle\ThirdPartyType\ThirdPartyTypeManager;
use Chill\MainBundle\Search\SearchInterface;

/**
 * 
 *
 */
class PickThirdPartyType extends AbstractType
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var ThirdPartyTypeManager
     */
    protected $typesManager;
    
    public function __construct(
        EntityManagerInterface $em, 
        UrlGeneratorInterface $urlGenerator, 
        TranslatorInterface $translator,
        ThirdPartyTypeManager $typesManager
    ) {
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
        $this->typesManager = $typesManager;
    }

    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('center')
            ->setAllowedTypes('center', [ \Chill\MainBundle\Entity\Center::class ])
            ->setDefined('types')
            ->setRequired('types')
            ->setAllowedValues('types', function($types) {
                if (FALSE === \is_array($types)) {
                    return false;
                }
                // return false if one element is not contained in allowed types
                return count(\array_diff($types, $this->typesManager->getTypes())) === 0;
            })
            ;
        
        $resolver
            ->setDefault('class', ThirdParty::class)
            ->setDefault('choice_label', function(ThirdParty $tp) {
                return $tp->getName();
            })
            ->setDefault('choice_loader', function(Options $options) {
                return new ThirdPartyChoiceLoader($options['center'], 
                    $this->em->getRepository(ThirdParty::class));
            })
            ;
    }
    
    public function getParent(): string
    {
        return EntityType::class;
    }
    
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
        $view->vars['attr']['class'] = \array_merge(['select2 '], $view->vars['attr']['class'] ?? []);
        $view->vars['attr']['data-3party-picker'] = true;
        $view->vars['attr']['data-select-interactive-loading'] = true;
        $view->vars['attr']['data-search-url'] = $this->urlGenerator
            ->generate('chill_main_search', [
                'name' => ThirdPartySearch::NAME,
                SearchInterface::REQUEST_QUERY_KEY_ADD_PARAMETERS => ['t' => $options['types']],
                '_format' => 'json' ]
                );
        $view->vars['attr']['data-placeholder'] = $this->translator->trans($options['placeholder']);
        $view->vars['attr']['data-no-results-label'] = $this->translator->trans('select2.no_results');
        $view->vars['attr']['data-error-load-label'] = $this->translator->trans('select2.error_loading');
        $view->vars['attr']['data-searching-label'] = $this->translator->trans('select2.searching');
    }
}
